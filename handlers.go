package main

import (
	"net/http"
	"fmt"
	"strconv"
	"html/template"
)

func indexHandler(w http.ResponseWriter, r *http.Request, vl *VotingList) {
	t := template.Must(template.New("vote.html").ParseFiles("templates/vote.html"))
	err := t.Execute(w, *vl)
	if err != nil {
		fmt.Fprintf(w, "%s\n", err)
	}

}

func newVoteHandler(w http.ResponseWriter, r *http.Request, vl *VotingList) {
	if r.Method != "POST" {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "Not a POST request.\n")
		return
	}
	
	// Create a Poll from POSTed data
	r.ParseForm()
	var p Poll

	if len(r.Form["title"]) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "No title provided.\n")
		return
	}

	if len(r.Form["options"]) <= 1 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "Not enough options provided.\n")
		return
	}

	p.Title = r.Form["title"][0]
	p.ID = vl.NextID
	vl.NextID++
	for i, option := range r.Form["options"] {
		p.Options = append(p.Options, &Options{Option: option, ID: i})
	} 
	// Append it to VotingList.AllPolls
	vl.AllPolls = append(vl.AllPolls, &p)
	http.Redirect(w, r, "/", 301)
}

func removeHandler(w http.ResponseWriter, r *http.Request, vl *VotingList) {
	if r.Method != "POST" {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "Not a POST request.\n")
		return
	}

	r.ParseForm()
	if len(r.Form["pollid"]) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "No pollid provided.\n")
		return
	}
	pollid, err := strconv.Atoi(r.Form["pollid"][0])
	
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "Invalid pollid.\n")
		return
	}
	
	// Find id in the VotingList
	for i, poll := range vl.AllPolls {
		if poll.ID == pollid {
			// Remove it from the VotingList
			vl.AllPolls = append(vl.AllPolls[:i], vl.AllPolls[i+1:]...)
			// woo, slice magic! 
		}
	}
	http.Redirect(w, r, "/", 301)
}

func castVoteHandler(w http.ResponseWriter, r *http.Request, vl *VotingList) {
	if r.Method != "POST" {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "Not a POST request.\n")
		return
	}
	// Find the Poll
	r.ParseForm()
	if len(r.Form["pollid"]) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "No pollid provided.\n")
		return
	}
	pollid, err := strconv.Atoi(r.Form["pollid"][0])
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "Invalid pollid.\n")
		return
	}

	if len(r.Form["optionid"]) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "No optionid provided.\n")
		return
	}
	
	optionid, err := strconv.Atoi(r.Form["optionid"][0])
	
	if len(r.Form["name"]) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "No name provided.\n")
		return
	}
	
	for _, poll := range vl.AllPolls {
		if poll.ID == pollid { // Found the poll! 
			v := Vote{Voter: r.Form["name"][0]}
			for _, option := range poll.Options {
				if option.ID == optionid { // Found the option! 
					option.Votes = append(option.Votes, &v)
				}
			}
		}
	}
	http.Redirect(w, r, "/", 301)
}
