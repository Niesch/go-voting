package main

type VotingList struct {
	AllPolls   []*Poll
	NextID     int	
}

type Poll struct {
	Title string
	Options []*Options
	ID int
}

type Options struct {
	Option string
	Votes  []*Vote
	ID     int
}

type Vote struct {
	Voter    string
}
